// 3 - I take a string, and console.log this string
function say(word) {                    // arg1(arg2)
  console.log(word);                    // === console.log(arg2)
}

// 2 - I expect callback and string, so I can pass the string into that callback
//     but i don't know what's gonna happen inside...
function execute(someFunction, value) { // function execute ( arg1, arg2 )
  someFunction(value);                  // === arg1(arg2)
}

// 1 - Instruction to run a function, passing arguments: function and string
//     we assume >> say("hello")
execute(say, "Hello");                  // execute( arg1, arg2 ) === arg1(arg2) === console.log(arg2)

// ---------------------------------
// let's rewrite "execute" with anonymous function:
// insert second argument everywhere in first argument - function
execute(function(word){ console.log(word) }, "Hello");