var http = require('http');
var url = require('url');

function start( route, handle ) {   // route - receive function via DI (dependency injection)

    http.createServer(function (request, response) {
        // request.url === http://localhost:8888/?bla=bla
        var pathname = url.parse(request.url).pathname;     // on main page is: "/"
        console.log( pathname );

        response.writeHead(200, {"Content-Type": "text/plain"}); // not writeHeader, not {"type": "plain-text"}
        var content = route( handle, pathname );
        response.write( content );
        response.end();

    }).listen(8888);

    console.log( "server running..." );
}

exports.start = start;